from django.apps import AppConfig


class BizAuthConfig(AppConfig):
    name = 'biz_auth'
