from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

# Create your models here.
class BizUser(User):
    business_name = models.CharField(_("business name"), max_length=150, blank=True)
    class Meta(User.Meta):
        verbose_name = _('biz_user')
        verbose_name_plural = _('biz_user')