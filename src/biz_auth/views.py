import json
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

# Create your views here.
def index(request):
    return HttpResponse("It works finally!")

def create_user(request):
    decoded_body = request.body.decode('utf-8')
    body = json.loads(decoded_body)
    print(body.get('name',False))
    return JsonResponse(body)